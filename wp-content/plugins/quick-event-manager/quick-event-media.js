jQuery(document).ready(function () {
    var custom_uploader;
    jQuery('.qem-color').wpColorPicker();
    jQuery('#upload_media_button').click(function (e) {
        e.preventDefault();
        if (custom_uploader) {custom_uploader.open(); return; }
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Select Background Image', button: {text: 'Insert Image'}, multiple: false});
        custom_uploader.on('select', function () {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            jQuery('#upload_image').val(attachment.url);
        });
        custom_uploader.open();
    });
    jQuery('#upload_submit_button').click(function (e) {
        e.preventDefault();
        if (custom_uploader) {custom_uploader.open(); return; }
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Select Submit Button Image', button: {text: 'Insert Image'}, multiple: false });
        custom_uploader.on('select', function () {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            jQuery('#submit_image').val(attachment.url);
        });
        custom_uploader.open();
    });
    jQuery('#upload_event_image').click(function (e) {
        e.preventDefault();
        if (custom_uploader) {custom_uploader.open(); return; }
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Select Event Image', button: {text: 'Insert Image'}, multiple: false});
        custom_uploader.on('select', function () {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            jQuery('#event_image').val(attachment.url);
        });
        custom_uploader.open();
    });
    jQuery("#yourplaces").keyup(function () {
        var model= document.getElementById('yourplaces');
        var number = jQuery('#yourplaces').val()
        if (number == 1)
                jQuery("#morenames").hide();
            else {
                jQuery("#morenames").show();
            }
    });
});