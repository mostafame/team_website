<?php
/**
 * @package TPL_PORTFOLIO
 * @version 1.1
 */

/**
 * Plugin Name: TLP Portfolio
 * Plugin URI: http://demo.radiustheme.com/wordpress/plugins/tlp-portfolio/
 * Description: Fully Responsive and Mobile Friendly Portfolio display plugin for wordpress with Grid and Isotope View.
 * Author: RadiusTheme
 * Version: 1.5
 * Author URI: https://radiustheme.com
 * Tag: best portfolio plugin, creative portfolio, portfolio, portfolio gallery, portfolio slider, Responsive Portfolio, wordpress portfolio plugin, wp portfolio
 * Text Domain: tlp-portfolio
 * Domain Path: /languages
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

define('TLP_PORTFOLIO_PLUGIN_PATH', dirname(__FILE__));
define('TLP_PORTFOLIO_PLUGIN_ACTIVE_FILE_NAME', plugin_basename( __FILE__ ) );
define('TLP_PORTFOLIO_PLUGIN_URL', plugins_url('', __FILE__));
define('TLP_PORTFOLIO_LANGUAGE_PATH', dirname( plugin_basename( __FILE__ ) ) . '/languages');

require ('lib/init.php');
