<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Tora
 */

get_header(); ?>
<style>
	.mass{
		font-size:400;
		color: black;
		margin-bottom:200px;
	}
	.main-not{

		margin-bottom: 200px;
		text-align: right;
	}
	.button-back{
		font-size: large;
		margin: 50px;

		opacity: 0.7;
		width: 250px;
	}
	.button-back:hover{
		background-color:indianred;
		color: ghostwhite;
		opacity: 1;
	}
	.buttons{
		display: block;
	}
</style>
	<div id="primary" class="fullwidth">
		<main id="main" class="site-main" role="main">
			<div class="main-not">
			<h1 class="mass">متاسفانه مطلب مورد نظر یافت نشد</h1>
			<div class="buttons">
				<button class="button-back">
					برگشت به صفحه اصلی
				</button>
				<button class="button-back">گزارش خرابی </button>
			</div>
			</div>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>