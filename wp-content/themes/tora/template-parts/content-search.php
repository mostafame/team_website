<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tora
 */

?>
<script>
	document.getElementById("testi").innerHTML="نتیجه جستجو"
</script>
<style>
	.page-header{
		text-align: right;
	}
	article{
		text-align: right;
	}
</style>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-meta">
		<?php tora_entry_meta(); ?>
	</div>
	
	<div class="search-inner">
		<header class="entry-header">
			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		</header><!-- .entry-header -->

		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
	</div>
</article><!-- #post-## -->

