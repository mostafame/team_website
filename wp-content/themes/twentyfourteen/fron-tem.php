<?php
/**
 * Created by PhpStorm.
 * User: roozbeh
 * Date: 7/17/2016
 * Time: 3:47 AM
 *Template Name: front-tem
 */
?>
<?php
get_header();
?>
<style>
    html{
        background-color: #7a7a7a;
    }
    #main-content{
        background-color: #7a7a7a;
    }
</style>

    <div id="main-content" class="main-content">

        <?php
        if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
            // Include the featured content template.
            get_template_part( 'featured-content' );
        }
        ?>

        <div id="primary" class="content-area">
            <div id="content" class="site-content" role="main">

                <?php

                    // Previous/next post navigation.
                    twentyfourteen_paging_nav();

                    //get_template_part( 'content', 'none' );
                ?>

            </div><!-- #content -->
        </div><!-- #primary -->
           </div><!-- #main-content -->

<?php
//get_sidebar();
get_footer();