<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'reznov');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'opNoj 0+U5-wK*9tgdB#}DU&b246SzXBcZP]o?cN:S,YqHQuT0[~F4ky!W|$_WJo');
define('SECURE_AUTH_KEY',  '`z;<bdYgzZ,+/&~BieJr^4VW~-l)3E4;6$1vYz.Q!D]?<G]oDw,wlX+iuQGoz~S^');
define('LOGGED_IN_KEY',    'TGe:Y4}:3wbQRb@RXHx0-nS.Cm[Kpt?%&#G$JUctE/a2_Q2|-[42H-PDF[8XmSYJ');
define('NONCE_KEY',        '9(?Ax7%iS(7K6pa0z^kz3s~;Sc?7f,7a[` H-swnpw.C_b8K$T2HJ,]pr49_X#V+');
define('AUTH_SALT',        ':W{8gI8{2JSOp+940?$Agj?*m|05DoNgU&2x.D%~&0>mETD^w@<OZ#rm/~+j~GVz');
define('SECURE_AUTH_SALT', '2[}<1EW`kIlm?s`g4~3?|+qU_n$V;0SV]|N{[WPQ84?V)Zy*zM2DAlc<:);q5?k?');
define('LOGGED_IN_SALT',   'L lzBG9j|CEyngD|)tb>Yl:walK%0Qy@4t|m5|e^y.P:UVuot/Bgu/K1Cr7`+drt');
define('NONCE_SALT',       'j>lGus+GP=yWv|S#-4E ]t)76.[>$U7(+Z>^_v$4R`;^|w8!@l @{L,1A>!tv^;5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
